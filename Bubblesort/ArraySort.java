import java.util.ArrayList;

public class ArraySort {
	static ArrayList<Integer> arrList = new ArrayList<Integer>();
	static int help;

	public static void main(String[] args) {
		arrList.add(3);
		arrList.add(39);
		arrList.add(9);
		arrList.add(65);
		arrList.add(23);
		help = arrList.get(0);

		for (int element : bubblesort(arrList)) {
			System.out.println(element);
		}

	}

	public static ArrayList<Integer> bubblesort(ArrayList<Integer> arr) {
		int help = 0;
		for (int i = 1; i < arr.size(); i++) {
			for (int j = 0; j < arr.size() - i; j++) {
				if (arr.get(j) > arr.get(j + 1)) {
					help = arr.get(i);
					arr.set(j, arr.get(j + 1));
					arr.set(j + 1, help);
				}

			}
		}
		return arr;
	}
}
