
public class BinarySearch {

	public static void main(String[] args) {
		int[] arr = {0, 4, 6, 9, 10};
		System.out.println(bs(arr, 5));
	}

	public static int bs(int[] arr, int value) {
		return bsPrivate(arr, value, 0, arr.length-1);
	}

	public static int bsPrivate(int[] arr, int value, int min, int max) {
		int length = min + (max - min) / 2;
		
		if (arr.length == 0) {
			return -1;
		}
		
		if ((max-min <= 1) && (min != value) && (max != value)){
			return -1;
		}
		
		if (value == arr[length]) {
			return length;
		}

		if (value < arr[length]) {
			return bsPrivate(arr, value, min, length - 1);
		}

		if (value > arr[length]) {
			return bsPrivate(arr, value, length + 1, max);
		}
		return -1;
	}
}
