import java.awt.EventQueue;

import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.WindowConstants;

import java.awt.Color;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;

import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;

public class Listung extends JFrame {

	private DBManager db;
	private Detail det;

	DefaultListModel<String> model1;
	DefaultListModel<String> model;
	DefaultListModel<String> model2;

	public Listung() throws SQLException {
		// setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Listung");
		setBounds(100, 100, 817, 453);
		getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 801, 397);
		getContentPane().add(panel);
		panel.setLayout(null);

		model1 = new DefaultListModel<String>();
		JList<String> list_1 = new JList<String>(model1);
		// list_1.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		list_1.setBounds(306, 69, 195, 235);
		panel.add(list_1);

		model2 = new DefaultListModel<String>();
		JList<String> list_2 = new JList<String>(model2);
		list_2.setBounds(591, 69, 195, 235);
		panel.add(list_2);

		model = new DefaultListModel<String>();
		JList<String> list = new JList<String>(model);
		list.setBounds(15, 69, 195, 235);
		panel.add(list);

		JButton btnNewButton = new JButton("<<");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!list_2.isSelectionEmpty()) {
					String s = list_2.getSelectedValue();
					model1.addElement(s);
					int index = list_2.getSelectedIndex();
					model2.removeElementAt(index);

					try {
						db.updateDoneDoing(s);
					} catch (SQLException ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		btnNewButton.setBounds(516, 220, 57, 44);
		panel.add(btnNewButton);

		JButton btnNewButton_1 = new JButton(">>");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!list_1.isSelectionEmpty()) {
					String s = list_1.getSelectedValue();
					model2.addElement(s);
					int index = list_1.getSelectedIndex();
					model1.removeElementAt(index);

					try {
						db.updateDoingDone(s);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});
		btnNewButton_1.setBounds(516, 123, 57, 44);
		panel.add(btnNewButton_1);

		JButton button = new JButton(">>");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!list.isSelectionEmpty()) {
					String s = list.getSelectedValue();
					model1.addElement(s);
					int index = list.getSelectedIndex();
					model.removeElementAt(index);

					try {
						db.updateTodoDoing(s);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});
		button.setBounds(225, 123, 57, 44);
		panel.add(button);

		JButton button_1 = new JButton("<<");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!list_1.isSelectionEmpty()) {
					String s = list_1.getSelectedValue();
					model.addElement(s);
					int index = list_1.getSelectedIndex();
					model1.removeElementAt(index);
					try {
						db.updateDoingTodo(s);
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		});
		button_1.setBounds(225, 220, 57, 44);
		panel.add(button_1);

		JLabel lblTodo = new JLabel("ToDo");
		lblTodo.setBounds(72, 33, 69, 20);
		panel.add(lblTodo);

		JLabel lblDoing = new JLabel("Doing");
		lblDoing.setBounds(372, 33, 69, 20);
		panel.add(lblDoing);

		JLabel lblDone = new JLabel("Done");
		lblDone.setBounds(655, 33, 69, 20);
		panel.add(lblDone);

		JButton btnAktualisieren = new JButton("Aktualisieren");
		btnAktualisieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					update();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		});
		btnAktualisieren.setBounds(189, 337, 134, 44);
		panel.add(btnAktualisieren);

		JButton btnAnzeigen = new JButton("Anzeigen");
		btnAnzeigen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					if (!list.isSelectionEmpty()) {
						det = new Detail(list.getSelectedValue(), db.selectFromHead("todo", list.getSelectedValue()));
					} else if (!list_1.isSelectionEmpty()) {
						det = new Detail(list_1.getSelectedValue(), db.selectFromHead("todo", list.getSelectedValue()));
					} else if (!list_2.isSelectionEmpty()) {
						det = new Detail(list_2.getSelectedValue(), db.selectFromHead("todo", list.getSelectedValue()));
					}
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				det.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				det.setVisible(true);
			}
		});
		btnAnzeigen.setBounds(492, 337, 134, 44);
		panel.add(btnAnzeigen);

		try {
			update();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void update() throws SQLException {
		model.clear();
		model2.clear();
		model1.clear();

		for (String element : db.selectHeadToDo()) {
			model.addElement(element);
		}

		for (String element : db.selectDoing()) {
			model1.addElement(element);
		}

		for (String element : db.selectDone()) {
			model2.addElement(element);
		}

	}
}
