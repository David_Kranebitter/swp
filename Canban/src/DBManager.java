import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Scanner;

	public class DBManager {

		private static Connection c;
		private static DBManager db;
		private String pw = "Iquoxum321";
		private String databaseName = "canban";

		DBManager() throws SQLException {
			try {
				Class.forName("com.mysql.jdbc.Driver");
				if (!(System.getProperty("user.dir").contains("David"))) {
					Scanner sc = new Scanner(System.in);
					System.out.println("Bitte Passwort eingeben: ");
					pw = sc.next();
					System.out.println("Bitte den Namen der Datenbank eingeben: ");
					databaseName = sc.next();
					sc.close();
				}
				c = DriverManager.getConnection("jdbc:mysql://localhost/" + databaseName, "root", pw);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}

		public static DBManager getInstance() throws SQLException {
			if (db == null) {
				db = new DBManager();
			}
			return db;
		}

		public static void create() {
			String sql = "";
			ArrayList<String> list = null;
			Statement stmt = null;
			Read r = new Read();
			try {
				stmt = c.createStatement();
				list = r.readIn();
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			for (int i = 0; i < list.size(); i++) {

				if (!(list.get(i).contains(");"))) {
					sql += list.get(i);
				} else {
					sql += list.get(i);
					try {
						stmt.executeUpdate(sql);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					sql = "";

				}

			}

		}

		public static void addStorycard(String text, String text2) {
			String sql = "Insert into canban (head, toDo) values (?,?)";
			PreparedStatement stmt = null;
			try {
				stmt = c.prepareStatement(sql);
				stmt.setString(1, text);
				stmt.setString(2, text2);
				stmt.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				if (stmt != null) {
					try {
						stmt.close();
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
			}
		}
		public static ArrayList<String> selectHeadToDo() throws SQLException {
			String sql = "Select head from canban where todo IS NOT NULL";
			Statement stmt = null;
			ArrayList<String> list = new ArrayList<String>();
			
			try {
				stmt = c.createStatement();
				
			}catch(SQLException e) {
				e.printStackTrace();
			}
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				list.add(rs.getString(1));
				
			}
			return list;
		}

		public static ArrayList<String> selectToDo() throws SQLException {
			String sql = "Select todo from canban";
			Statement stmt = null;
			ArrayList<String> list = new ArrayList<String>();
			
			try {
				stmt = c.createStatement();
				
			}catch(SQLException e) {
				e.printStackTrace();
			}
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				list.add(rs.getString(1));
				
			}
			return list;
		}

		public static ArrayList<String> selectDoing() throws SQLException {
			String sql = "Select doing from canban";
			Statement stmt = null;
			ArrayList<String> list = new ArrayList<String>();
			
			try {
				stmt = c.createStatement();
				
			}catch(SQLException e) {
				e.printStackTrace();
			}
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) {
				list.add(rs.getString(1));
				
			}
			return list;
		}
		
		public static ArrayList<String> selectDone() throws SQLException {
			String sql = "Select done from canban";
			Statement stmt = null;
			ArrayList<String> list = new ArrayList<String>();
			
			try {
				stmt = c.createStatement();
				
			}catch(SQLException e) {
				e.printStackTrace();
			}
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) 
			{
				list.add(rs.getString(1));
				
			}
			return list;
		}


		
		public static void updateTodoDoing(String s) throws SQLException {
			Statement stmt = null;
			String sql = "Update canban set head = null, doing = \"" + s + "\" where head = \"" + s + "\"";
			
				stmt = c.createStatement();
				stmt.executeUpdate(sql);
		}
		
		public static void updateDoingDone(String s) throws SQLException {
			Statement stmt = null;
			String sql = "Update canban set doing = null, done = \"" + s + "\" where doing = \"" + s + "\"";
			
				stmt = c.createStatement();
				stmt.executeUpdate(sql);
		}
		
		public static void updateDoneDoing(String s) throws SQLException {
			Statement stmt = null;
			String sql = "Update canban set done = null, doing = \"" + s + "\" where done = \"" + s + "\"";
			
				stmt = c.createStatement();
				stmt.executeUpdate(sql);
		}
		
		public static void updateDoingTodo(String s) throws SQLException {
			Statement stmt = null;
			String sql = "Update canban set doing = null, head = \"" + s + "\" where doing = \"" + s + "\"";
			
				stmt = c.createStatement();
				stmt.executeUpdate(sql);
		}
		
		public static String selectFromHead (String row, String head) throws SQLException
		{
			String description = null;
			Statement stmt = null;
			String sql = "select "+ row +" from canban where head = \""+ head + "\"";
			
			stmt=c.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while(rs.next()) 
			{
				description = rs.getString(1);
//				System.out.println(description);
			}
			return description;
		}

		public void close() {
			try {
				c.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

		public static void main(String[] args) throws SQLException {
			getInstance();
			
			
		}

	}

	
