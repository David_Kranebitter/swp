import java.awt.Frame;

import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) {
		JFrame f = new JFrame();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Snake sf = new Snake();
		f.add(sf);
		f.setSize(500, 500);
		f.setVisible(true);
	}

}
