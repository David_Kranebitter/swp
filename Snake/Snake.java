import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Snake extends JPanel implements ActionListener, KeyListener {

	Timer t = new Timer(300, this);

	private JFrame frame;

	int x = 225;
	int y = 225;
	int xVel = 0;
	int yVel = 0;
	int[] xArr = { 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450 };
	int[] yArr = { 25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425 };
	ArrayList<Integer> xList = new ArrayList<Integer>();
	ArrayList<Integer> yList = new ArrayList<Integer>();

	int collected = 0;

	Random rnd = new Random();
	int rndx = rnd.nextInt(18);
	int rndy = rnd.nextInt(17);

	public Snake() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(this);
		frame.setSize(500, 500);
		frame.addKeyListener(this);
		frame.setFocusable(true);
		frame.setVisible(true);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.BLACK);
		g.setColor(Color.GREEN);
		g.fillRect(x, y, 25, 25);
		if (collision(x, y, xArr[rndx], yArr[rndy], xList, yList)) {
			collected++;
			do {
				rndx = rnd.nextInt(18);
				rndy = rnd.nextInt(17);
			} while (isOnP(rndx, rndy, xList, yList));
		}
		g.setColor(Color.RED);
		g.fillRect(xArr[rndx], yArr[rndy], 25, 25);

		if (collected != 0) {
			for (int i = 1; i <= collected; i++) {
				g.setColor(Color.GREEN);
				try {
					g.fillRect(xList.get(xList.size() - (i + 1)), yList.get(yList.size() - (i + 1)), 25, 25);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		t.start();
	}

	public boolean isOnP(int oX, int oY, ArrayList<Integer> pxList, ArrayList<Integer> pyList) {
		boolean status = false;
		for (int i = 0; i < pxList.size(); i++) {
			if (oX == pxList.get(i) && oY == pyList.get(i)) {
				status = true;
			}
		}

		return status;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (x < 0) {
			x = 475;
		}
		if (x > 475) {
			x = 0;
		}
		if (y < 0) {
			y = 475;
		}
		if (y > 475) {
			y = 0;
		}

		x += xVel;
		y += yVel;

		xList.add(x);
		yList.add(y);

		repaint();
	}

	public boolean collision(int pX, int pY, int oX, int oY, ArrayList<Integer> pxList, ArrayList<Integer> pyList) {
		boolean status = false;

		if ((pX == oX) && (pY == oY)) {
			status = true;
		}

		if (xList.size() > 1 && yList.size() > 1) {
			for (int i = 1; i <= collected; i++) {
				if (pX == xList.get(xList.size() - (i + 1)) && pY == yList.get(yList.size() - (i + 1))) {

					resetGame();
				}
			}
		}
		return status;
	}

	@Override
	public void keyPressed(KeyEvent e) {

		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			if (collected == 0 || ((y - 25) != yList.get(yList.size() - 2) && x != xList.get(xList.size() - 2))) {
				yVel = -25;
				xVel = 0;
			}
			break;
		case KeyEvent.VK_DOWN:
			if (collected == 0 || ((y + 25) != yList.get(yList.size() - 2) && x != xList.get(xList.size() - 2))) {
				yVel = 25;
				xVel = 0;
			}
			break;
		case KeyEvent.VK_LEFT:
			if (collected == 0 || (y != yList.get(yList.size() - 2) && (x - 25) != xList.get(xList.size() - 2))) {
				xVel = -25;
				yVel = 0;
			}
			break;
		case KeyEvent.VK_RIGHT:
			if (collected == 0 || (y != yList.get(yList.size() - 2) && (x + 25) != xList.get(xList.size() - 2))) {
				xVel = 25;
				yVel = 0;
			}
			break;

		}
	}

	public void resetGame() {
		x = 225;
		y = 225;
		xVel = 0;
		yVel = 0;

		xList = new ArrayList<Integer>();
		yList = new ArrayList<Integer>();

		collected = 0;

		rnd = new Random();
		rndx = rnd.nextInt(18);
		rndy = rnd.nextInt(17);
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void keyTyped(KeyEvent e) {

	}

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					new Snake();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

}
