
public class Fakultaet {
	
public static void main(String[] args) {
	
	System.out.println(retM(5));
}
	
	public static int rek(int value, int m) {
		if(value == 1) {
			return 1;
		}
		return value * rek(value-1, m );
	}
	
	public static int retM(int m) {
		return rek(m,1);
	}
	
	
}
