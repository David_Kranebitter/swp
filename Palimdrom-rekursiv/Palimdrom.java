
public class Palimdrom {

	public static void main(String[] args) {
		if (palimdrom("otto")) {
			System.out.println("TRUE");
		} else {
			System.out.println("FALSE");
		}
	}

	public static boolean palimdrom(String s) {
		if (s.length() == 0 || s.length() == 1) {
			return true;
		}
		if (s.charAt(0) == s.charAt(s.length() - 1)) {
			return palimdrom(s.substring(1, s.length() - 1));
		}
		return false;
	}
}
