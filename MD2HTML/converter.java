import java.util.ArrayList;

public class converter {
	private ArrayList<String> list;
	private ArrayList<String> convertedList = new ArrayList<String>();
	int runs = 0;
	int fPos = 0;
	int fListLength = 0;

	public converter(ArrayList<String> list) {
		this.list = list;
	}

	public ArrayList<String> convert() {
		convertedList.add("<html>");
		convertedList.add("<head> <title> MD2HTML </title> </head>");
		convertedList.add("<body>");

		for (int i = 0; i < list.size(); i++) {
			int type = identify(list.get(i));

			switch (type) {
			case 1:
				convertedList.add(cursivAndFat(list.get(i)));
				break;
			case 2:
				convertedList.add(titel(list.get(i)));
				break;
			case 3:
				convertedList.add(list(list.get(i), i, getListLenth(list, i)));
				break;
			}

		}
		convertedList.add("</body>");
		convertedList.add("</html>");

		return convertedList;

	}

	public int getSecStar(String line, int pos, int next) {
		int secStar = 0;
		System.out.println(pos);

		for (int i = pos + next; i < line.length(); i++) {
			if (line.charAt(i) == '*') {
				secStar = i;
				break;
			}

		}

		return secStar;
	}

	public String cursivAndFat(String line) {
		int fak = 0;
		boolean b = false;
		StringBuffer sb = new StringBuffer(line);
		String convertedLine = "";

		for (int i = 0; i < line.length(); i++) {
			// if (i != line.length() - 1 || i != line.length()) {
			try {
				if (!(i > line.length() - 2)) {
					if (line.charAt(i) == '*' && !b && line.charAt(i + 1) != '*') {

						if (i != 0) {
							fak = 1;
						} else {
							fak = 0;
						}
						if (line.charAt(i - fak) != '*' || i == 0) {
							final int next = 1;
							StringBuffer sbr = new StringBuffer(line);
							b = true;
							String help = "";
							int pos = getSecStar(line, i, next);
							help = line.substring(i, pos + 1);
							help = help.replace("*", "<i>");
							sbr = new StringBuffer(help);
							help = sbr.insert(help.length()-2, '/').toString();
							convertedLine += help;
							if (line.length() != pos + 1) {
								i = pos + 1;
							} else {
								i = pos + 0;
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// if (i != line.length() - 2 || i != line.length() - 1 || i != line.length()) {
			try {
				if (!(i > line.length() - 2)) {

					if (line.charAt(i) == '*' && line.charAt(i + 1) == '*' && line.charAt(i + 2) != '*' && !b) {
						if (i != 0) {
							fak = 1;
						} else {
							fak = 0;
						}
						if (line.charAt(i - fak) != '*' || i == 0) {
							final int next = 2;
							StringBuffer sbr = new StringBuffer(line);
							b = true;
							String help = "";
							int pos = getSecStar(line, i, next);
							help = line.substring(i, pos + 2);
							help = help.replace("**", "<b>");
							sbr = new StringBuffer(help);
							help = sbr.insert(help.length()-2, '/').toString();
							convertedLine += help;
							if (line.length() != pos + 2) {
								i = pos + 2;
							} else {
								i = pos + 1;
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			// }
			// if (i != line.length() - 2 || i != line.length() - 1 || i != line.length()) {
			try {
				if (!(i > line.length() - 2)) {
					if (line.charAt(i) == '*' && line.charAt(i + 1) == '*' && line.charAt(i + 2) == '*' && !b) {
						if (i != 0) {
							fak = 1;
						} else {
							fak = 0;
						}
						if (line.charAt(i - fak) != '*' || i == 0) {
							final int next = 3;
							StringBuffer sbr = new StringBuffer(line);
							b = true;
							String help = "";
							int pos = getSecStar(line, i, next);
							help = line.substring(i, pos + 3);
							help = help.replace("***", "<i>");
							sbr = new StringBuffer(help);
							help = sbr.insert(help.length()-2, '/').toString();
							convertedLine += "<b>" + help + "</b>";
							if (line.length() != pos + 3) {
								i = pos + 3;
							} else {
								i = pos + 2;
							}
						}
						// fett&cursiv
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			if (line.charAt(i) != '*') {
				convertedLine += line.charAt(i);
			}

			b = false;
		}

		return convertedLine;
	}

	public String titel(String line) {
		String convertedLine = "";
		int iCounter = 0;
		String subtitel = "";
		for (int i = 0; i < line.length(); i++) {
			if (line.charAt(i) == '#') {
				iCounter++;
			}
		}
				
		for (int i = 0; i < iCounter / 2; i++) {
			subtitel += '#';
		}

		convertedLine = line.replace(subtitel, "<h" + iCounter / 2 + ">");
		StringBuffer sf = new StringBuffer(convertedLine);
		sf.insert(convertedLine.length() - 3, "/");

		return sf.toString();
	}

	public int identify(String line) {
		int iCounter = 0;
		line.replaceAll("//s+", "");

		if (line.contains("*")) {
			iCounter = 1;
			runs = 0;
		}

		if (line.trim().charAt(0) == '#') {
			iCounter = 2;
			runs = 0;
		}

		if (line.trim().charAt(0) == '-') {
			iCounter = 3;
			runs++;
		}

		return iCounter;
	}

	public String list(String line, int pos, int listLength) {
		if (fListLength == 0) {
			fListLength = listLength;
		}

		if (fPos == 0) {
			fPos = pos;
		}
		String convertedLine = "";
		String s = "";
		s = line.replace("-", "<li>");

		if (runs == 1) {
			convertedLine = "<ul> " + s + " </li>";
		}
		if (pos > fPos) {
			convertedLine = s + " </li>";
		}
		if (pos == fListLength + fPos - 1) {
			convertedLine = s + "</li>" + " </ul>";
			fPos = 0;
			fListLength = 0;
		}
		return convertedLine;
	}

	public ArrayList<String> getConvertedList() {
		return convertedList;
	}

	public void setConvertedList(ArrayList<String> convertedList) {
		this.convertedList = convertedList;
	}

	public int getListLenth(ArrayList<String> list, int pos) {
		int listLength = 0;

		for (int i = pos; i < list.size(); i++) {
			if (list.get(i).trim().charAt(0) == '-') {
				listLength++;
			} else {
				break;
			}
		}
		return listLength;
	}
}
