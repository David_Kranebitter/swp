import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JLabel;

public class Read
{
	private String path;
	private ArrayList<String> text;
	private boolean err = false;

	public Read(String path) {
		this.path = path;
	}

	public ArrayList<String> readIn() throws IOException {
		ArrayList<String> lines =  new ArrayList<String>();
		FileReader fr;
		BufferedReader br = null;
		String line = "";
		try {
			fr = new FileReader(path);
			br = new BufferedReader(fr);
			line = br.readLine();
		} catch (FileNotFoundException e1) {
			err = true;
			JDialog dialog = new JDialog();
			dialog.setTitle("Error");
			dialog.setSize(270,115);
			dialog.setModal(true);
			dialog.getContentPane().add(new JLabel("Datei existiert nicht!"));
			dialog.setVisible(true);
		}
		
		while (line != null && !err ) {
			if(!line.trim().isEmpty()) {
				lines.add(line);
			}
			try {
				line = br.readLine();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return lines;
	}

	public ArrayList<String> getText() {
		return text;
	}

	public void setText(ArrayList<String> text) {
		this.text = text;
	}
}