import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Write {

	private ArrayList<String> lines = new ArrayList<String>();
	private String path;

	public Write(ArrayList<String> lines, String path) {
		this.lines = lines;
		this.path = path;
	}

	public void WriteFile() throws IOException {
		try {
			if (!path.contains("\\")) {
				path = "";
			}

			FileWriter fw = new FileWriter(path);
			for (String element : lines) {
				fw.write(element);
				fw.append(System.getProperty("line.separator"));
			}
			fw.close();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
	}

	public boolean createFileINE(File file) {
		if (file != null) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (file.isFile() && file.canWrite() && file.canRead()) {
				return true;
			}
		}
		return false;
	}

}
