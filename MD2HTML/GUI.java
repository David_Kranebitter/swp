import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class GUI {

	private JFrame frmMdhtml;
	private JTextField textField;
	private JTextField textField_1;
	private Read r;
	private Write w;
	private converter c;
	
	private String OS = System.getProperty("os.name").toLowerCase();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI window = new GUI();
					window.frmMdhtml.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMdhtml = new JFrame();
		frmMdhtml.setTitle("MD2HTML");
		frmMdhtml.setBounds(100, 100, 450, 187);
		frmMdhtml.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMdhtml.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 428, 131);
		frmMdhtml.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblPfadAuswhlen = new JLabel(".md auswaehlen");
		lblPfadAuswhlen.setBounds(15, 16, 124, 20);
		panel.add(lblPfadAuswhlen);

		textField = new JTextField();
		textField.setBounds(135, 13, 139, 26);
		panel.add(textField);
		textField.setColumns(10);

		JButton btnDurchsuchen = new JButton("Durchsuchen");
		btnDurchsuchen.addActionListener(new ActionListener() {
			String path = "";
			public void actionPerformed(ActionEvent arg0) {
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new java.io.File("user.dir"));
				fc.setDialogTitle("Durchsuchen");

				if (fc.showOpenDialog(btnDurchsuchen) == JFileChooser.APPROVE_OPTION) {
					path = fc.getSelectedFile().getAbsolutePath();
					if (OS.indexOf("win") >= 0) {
						path = path.replace("/", "\\");
					}
				}
				textField.setText(path);
			}
		});
		btnDurchsuchen.setBounds(289, 12, 124, 29);
		panel.add(btnDurchsuchen);

		JButton btnKonvertieren = new JButton("Konvertieren");
		btnKonvertieren.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (textField.getText().length() > 0 && textField_1.getText().length() > 0) {
					if (textField_1.getText().contains("\\")) {

						try {
							r = new Read(textField.getText());
							c = new converter(r.readIn());
							w = new Write(c.convert(), textField_1.getText());
							w.createFileINE(new File(textField_1.getText()));
							w.WriteFile();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
					}else {
						JDialog dialog = new JDialog();
						dialog.setTitle("Error");
						dialog.setSize(270,115);
						dialog.setModal(true);
						dialog.getContentPane().add(new JLabel("Speicherort existiert nicht!"));
						dialog.setVisible(true);
					}

				} else {
					JDialog dialog = new JDialog();
					dialog.setTitle("Error");
					dialog.setSize(270, 115);
					dialog.setModal(true);
					dialog.getContentPane().add(new JLabel("Dateipfad und Speicherort angeben!"));
					dialog.setVisible(true);
				}
			}
		});

		btnKonvertieren.setBounds(15, 91, 124, 29);
		panel.add(btnKonvertieren);

		JLabel lblSpichernIn = new JLabel("Speichern in");
		lblSpichernIn.setBounds(15, 52, 124, 20);
		panel.add(lblSpichernIn);

		textField_1 = new JTextField();
		textField_1.setColumns(10);
		textField_1.setBounds(135, 49, 139, 26);
		panel.add(textField_1);

		JButton button = new JButton("Durchsuchen");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String path = "";
				JFileChooser fc = new JFileChooser();
				fc.setCurrentDirectory(new java.io.File(System.getProperty("user.home")));
				fc.setDialogTitle("Durchsuchen");

				if (fc.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					path = fc.getSelectedFile().getAbsolutePath() + ".html";
				}
				if (OS.indexOf("win") >= 0) {
					path = path.replace("/", "\\");
				}
				textField_1.setText(path);

			}
		});
		button.setBounds(289, 48, 124, 29);
		panel.add(button);
	}
}