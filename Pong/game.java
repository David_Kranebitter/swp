package testtest;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class game extends JPanel implements ActionListener, KeyListener {

	final int intervall = 20;
	boolean isSingleplayer;

	Timer t = new Timer(intervall, this);

	int x = 0;
	int y = 0;
	int pX1 = 600;
	int pY1 = 170;
	int pX2 = 50;
	int pY2 = 170;
	int speed = 5;
	int pSpeed = 8;
	int xVel = speed;
	int yVel = speed;
	int pXVel1 = 0;
	int pYVel1 = 0;
	int pXVel2 = 0;
	int pYVel2 = 0;
	int pointsP1 = 0;
	int pointsP2 = 0;

	int intervallCounter = 0;

	private JFrame frame;

	public game(boolean isSinglePlayer) {
		this.isSingleplayer = isSinglePlayer;
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(this);
		frame.setSize(700, 500);
		frame.addKeyListener(this);
		frame.setFocusable(true);
		frame.setVisible(true);
		this.setSize(new Dimension(700, 500));
		frame.getContentPane().add(this);
		setLayout(null);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.setBackground(Color.WHITE);
		g.setColor(Color.BLACK);

		Font f = new Font("Comic Sans MS", Font.BOLD, 25);
		g.setFont(f);
		g.drawString("P2:" + pointsP2 + " - P1:" + pointsP1, 250, 40);

		g.fillOval(x, y, 25, 25);
		g.fillRect(pX1, pY1, 20, 80);
		g.fillRect(pX2, pY2, 20, 80);

		t.start();
	}

	public boolean collisionP2(int x, int y, int pX2, int pY2) {

		for (int i = pY2 - 20; i < pY2 + 60; i++) {
			if (x <= pX2 + 12 && y == i) {
				return true;

			}
		}

		return false;
	}

	public boolean collisionP1(int x, int y, int pX1, int pY1) {

		for (int j = pY1 - 20; j < pY1 + 60; j++) {
			if (x >= pX1 - 25 && y == j) {
				return true;
			}
		} 
		return false;
	}

	@Override
	public void keyPressed(KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_UP) {
			pYVel1 = -pSpeed;
		}

		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			pYVel1 = pSpeed;
		}
		
		if (!isSingleplayer) {
			if (e.getKeyCode() == KeyEvent.VK_W) {
				pYVel2 = -pSpeed;
			}

			if (e.getKeyCode() == KeyEvent.VK_S) {
				pYVel2 = pSpeed;
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {

		if (e.getKeyCode() == KeyEvent.VK_UP) {
			pYVel1 = 0;
		}

		if (e.getKeyCode() == KeyEvent.VK_DOWN) {
			pYVel1 = 0;
		}

			if (e.getKeyCode() == KeyEvent.VK_W) {
				pYVel2 = 0;
			}

			if (e.getKeyCode() == KeyEvent.VK_S) {
				pYVel2 = 0;
			}
		

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {

		intervallCounter += intervall;

		if (intervallCounter % 2500 == 0) {
			speed++;
		}

		if (y + yVel + 80 > frame.getHeight()) {
			yVel = -speed;
		}

		if (y + yVel < 0) {
			yVel = speed;
		}

		if (x + xVel + 40 > frame.getWidth()) {
			speed = 5;
			intervallCounter = 0;
			x = 0;
			y = 0;
			xVel = speed;
			yVel = speed;
			pointsP2++;
		}

		if (x + xVel < 0) {
			speed = 5;
			intervallCounter = 0;
			x = 0;
			y = 0;
			xVel = speed;
			yVel = speed;
			pointsP1++;
		}

		if (isSingleplayer) {

			if (pY2 + 40 < y)
				pYVel2 = pSpeed;

			if (pY2 + 40 > y)
				pYVel2 = -pSpeed;

			if (xVel == speed) {
				pYVel2 = 0;
			}
		}

		if (collisionP1(x, y, pX1, pY1))
			xVel = -speed;

		if (collisionP2(x, y, pX2, pY2))
			xVel = speed;

		x += xVel;
		y += yVel;

		pX1 += pXVel1;
		pY1 += pYVel1;

		pX2 += pXVel2;
		pY2 += pYVel2;

		repaint();
	}
}
