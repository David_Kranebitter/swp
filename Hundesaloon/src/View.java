import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class View {

	private JFrame frmHundesaloon;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_4;
	JComboBox<Hund> comboBox = new JComboBox<Hund>();
	JButton btnVorstellen = new JButton("Vorstellen");
	JButton btnNewButton = new JButton("+");

	/**
	 * Launch the application.
	 */

	/**
	 * Create the application.
	 */
	public View() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmHundesaloon = new JFrame();
		frmHundesaloon.setTitle("Hundesaloon");
		frmHundesaloon.setBounds(100, 100, 479, 385);
		frmHundesaloon.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmHundesaloon.getContentPane().setLayout(null);
		frmHundesaloon.setVisible(true);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 457, 329);
		frmHundesaloon.getContentPane().add(panel);
		panel.setLayout(null);

		JLabel lblNewLabel = new JLabel("Hinzuf\u00FCgen:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel.setBounds(15, 16, 158, 32);
		panel.add(lblNewLabel);

		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(25, 52, 69, 20);
		panel.add(lblName);

		textField = new JTextField();
		textField.setBounds(109, 49, 204, 23);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblAlter = new JLabel("Alter:");
		lblAlter.setBounds(25, 88, 69, 20);
		panel.add(lblAlter);

		textField_1 = new JTextField();
		textField_1.setBounds(109, 85, 204, 26);
		panel.add(textField_1);
		textField_1.setColumns(10);

		JLabel lblRasse = new JLabel("Rasse:");
		lblRasse.setBounds(25, 124, 69, 20);
		panel.add(lblRasse);

		textField_2 = new JTextField();
		textField_2.setBounds(109, 121, 204, 23);
		panel.add(textField_2);
		textField_2.setColumns(10);

		btnNewButton = new JButton("+");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

			}
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 30));
		btnNewButton.setBounds(328, 48, 115, 96);
		panel.add(btnNewButton);

		JSeparator separator = new JSeparator();
		separator.setBounds(15, 160, 428, 1);
		panel.add(separator);

		JLabel lblNewLabel_1 = new JLabel("Vorstellen:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblNewLabel_1.setBounds(15, 177, 163, 20);
		panel.add(lblNewLabel_1);

		JLabel lblHund = new JLabel("Hund:");
		lblHund.setBounds(25, 213, 69, 20);
		panel.add(lblHund);

		btnVorstellen = new JButton("Vorstellen");
		btnVorstellen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

			}
		});
		btnVorstellen.setBounds(15, 254, 428, 29);
		panel.add(btnVorstellen);

		comboBox = new JComboBox<Hund>();
		comboBox.setBounds(109, 210, 170, 26);
		panel.add(comboBox);

		textField_4 = new JTextField();
		textField_4.setBounds(15, 299, 428, 23);
		panel.add(textField_4);
		textField_4.setColumns(10);
	}

	public void setText(String s) {
		textField_4.setText(s);
	}

	public Hund getSelectedItem() {
		return (Hund) comboBox.getSelectedItem();
	}

	public void addIntroduceListener(ActionListener a) {
		btnVorstellen.addActionListener(a);
	}
	
	public void addAddListener(ActionListener a) {
		btnNewButton.addActionListener(a);
	}
	
	public String getName() {
		return textField.getText();
	}
	
	public int getAlter() {
		return Integer.parseInt(textField_1.getText());
	}
	
	public String getRasse() {
		return textField_2.getText();
	}
	
	public void setVisible() {
		frmHundesaloon.setVisible(true);
	}
}
