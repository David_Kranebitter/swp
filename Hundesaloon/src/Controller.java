import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

	private static View view = new View();
	private Hund model;
	private static Controller controller;

	private Controller(View view) {
		this.view = view;

		init();
	}

	public void init() {
		view.addAddListener(new AddListener());
		view.addIntroduceListener(new IntroduceListener());
	}

	public static Controller getInstance() {
		if (Controller.controller == null) {
			if (Controller.view == null) {
				view = new View();
			}
			Controller.controller = new Controller(view);
		}
		return Controller.controller;
	}

	public class IntroduceListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			view.setText(view.getSelectedItem().getSpruch());
		}
	}

	public class AddListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			model = new Hund(view.getName(), view.getAlter(), view.getRasse());
			view.comboBox.addItem(model);
		}
	}

	public static void main(String[] args) {
		controller = Controller.getInstance();
	}
}
