
public class Hund {
	
	private String name;
	private int alter;
	private String rasse;
	private String spruch;

	public Hund(String name, int alter, String rasse) {
		super();
		this.name = name;
		this.alter = alter;
		this.rasse = rasse;
		this.spruch = "Hallo ich bin " + name + " und bin ein " + rasse;
	}
	
	public String toString() {
		return name;
	}
	
	public String getSpruch() {
		return spruch;
	}
	
	

}
